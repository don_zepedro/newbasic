<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Categories';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="categories-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Categories', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?php //  foreach($dataProvider as $data=>$k){print_r($k);echo "<br>";} die; ?>
    <?php     \yii\widgets\Pjax::begin() ?>
        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],
               'id',
                [
                    'label'=>'Categories text',
                    'format'=>'raw',
                    'value'=>function($data){
                    return Html::a($data['categories_text'],'../../by-categories/listbycategories'.'?rec_id='.$data['records_id']);
                    }
                ],
                
//                'categories_text:ntext',
                'records_id',

                ['class' => 'yii\grid\ActionColumn'],
            ],
        ]); ?>
    <?php     \yii\widgets\Pjax::end() ?>


</div>
