<?php

use yii\db\Migration;

/**
 * Class m210429_023718_tables
 */
class m210429_023718_tables extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->execute("PRAGMA foreign_keys=on");
//        $this->checkIntegrity(true);
        $this->createTable("records", [
           'id' => $this->primaryKey()->notNull(),
            'title' => $this->string(),
            'description' => $this->text()
        ]);
//        $this->createTable("categories", [
//            'id'=>$this->primaryKey()->notNull(),
//            'record_id'=>$this->integer(),
//            'categories_text' => $this->text()->notNull(),
//            'record_id'=>'FOREIGN KEY record_id REFERENCES records(id)'
//        ]);
        $this->createTable("categories",[ 
            'id' => $this->primaryKey()->notNull(),
            'categories_text' => $this->text()->notNull(),
            'records_id'=>'integer NOT NULL REFERENCES records(id) ON UPDATE CASCADE ON DELETE CASCADE'
            ]);
//            'record_id'=>$this->integer()->notNull()
//                array(
//            'id'=>'pk',
//            'categories_text'=>'text NOT NULL',
//            'records_id'=>'integer NOT NULL REFERENCES records(id) ON UPDATE CASCADE ON DELETE CASCADE'
//        ));
       
//        $this->createIndex('idx-record-rec-id', 'records', 'id');
        
//        $this->addForeignKey('fr-records-record-id', 'categories', 'record_id', 'records', 'id','CASCADE','CASCADE');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
          $this->dropTable("records");
          $this->dropTable("categories");

    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m210429_023718_tables cannot be reverted.\n";

        return false;
    }
    */
}
