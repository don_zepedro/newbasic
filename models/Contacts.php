<?php

namespace app\models;

use Yii;
/*
/**
 * This is the model class for table "contacts".
 *
 * @property int $Employee ID
 * @property string $User Name
 * @property string $Name Prefix
 * @property string $First Name
 * @property string $Middle Initial
 * @property string $Last Name
 * @property string $Gender
 * @property string $E-Mail
 * @property string $Date of Birth
 * @property string $Time of Birth
 * @property int $Age in Yrs.
 * @property string $Date of Joining
 * @property int $Age in Company (Years)
 * @property int $Phone No.
 * @property string $Place Name
 * @property string $Country
 * @property string $City
 * @property int $Zip
 * @property string $Region
 */
class Contacts extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'contacts';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['User Name', 'Name Prefix', 'First Name', 'Middle Initial', 'Last Name', 'Gender', 'E-Mail', 'Date of Birth', 'Time of Birth', 'Age in Yrs.', 'Date of Joining', 'Age in Company (Years)', 'Phone No.', 'Place Name', 'Country', 'City', 'Zip', 'Region'], 'required'],
            [['Date of Birth', 'Time of Birth', 'Date of Joining'], 'safe'],
            [['Age in Yrs.', 'Age in Company (Years)', 'Phone No.', 'Zip'], 'integer'],
            [['User Name', 'Name Prefix', 'First Name', 'Middle Initial', 'Last Name', 'Gender', 'E-Mail', 'Place Name', 'Country', 'City', 'Region'], 'string', 'max' => 50],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'Employee ID' => 'Employee ID',
            'User Name' => 'User Name',
            'Name Prefix' => 'Name Prefix',
            'First Name' => 'First Name',
            'Middle Initial' => 'Middle Initial',
            'Last Name' => 'Last Name',
            'Gender' => 'Gender',
            'E-Mail' => 'E Mail',
            'Date of Birth' => 'Date Of Birth',
            'Time of Birth' => 'Time Of Birth',
            'Age in Yrs.' => 'Age In Yrs',
            'Date of Joining' => 'Date Of Joining',
            'Age in Company (Years)' => 'Age In Company ( Years)',
            'Phone No.' => 'Phone No',
            'Place Name' => 'Place Name',
            'Country' => 'Country',
            'City' => 'City',
            'Zip' => 'Zip',
            'Region' => 'Region',
        ];
    }
}
