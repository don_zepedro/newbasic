<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace app\commands;

use GuzzleHttp\Client;
use yii\console\Controller;
use Yii;
use yii\console\ExitCode;
use phpQuery;
use app\models\Records;
use app\models\Categories;
/**
 * Description of ParseController
 *
 * @author zepedro
 */
class ParseController extends Controller
{
    public function actionMyParse(){
        
        $model = new Records();
        $catmodel = new Categories();
        $catmodel->records_id = $model->find()->orderBy(['id'=>SORT_DESC])->one();
        $client = new Client(['base_uri' => 'https://www.kinopoisk.ru']);
        $response = $client->get('/media/news/4004367/');
        $content = $response->getBody()->getContents();
        $phpquery =phpQuery::newDocument($content);
        $title = $phpquery->find('title')->html();
        $description = preg_replace('/\s?<a[^>]*?>.*?<\/a>\s?/si','',$phpquery->find('p.stk-reset')->html());
        $description = preg_replace('/\s?<em[^>]*?>.*?<\/em>\s?/si','',$description);
        $model->title = $title;
        $model->description = $description;
        $catmodel->records_id = $model->find()->orderBy(['id'=>SORT_DESC])->one();
        ($catmodel->records_id == NULL) ? $catmodel->records_id=1 : $catmodel->records_id = $catmodel->records_id['id'] + 1;
        $catmodel->categories_text = $title;
        $this->tryToSave($model);
        $this->tryToSave($catmodel);
        
        
        Yii::$app->mailer->compose()
        ->setFrom('zepedro@yandex.ru')
        ->setTo('zepedro@yandex.ru')
        ->setSubject('Тема сообщения')
        ->setTextBody('Текст сообщения')
        ->setHtmlBody('<b>текст сообщения в формате HTML</b>')
        ->send();
        
    }
    public function tryToSave($model){
        try{
            $model->save();
        }catch (\Exception $e){
            print_r(array([
                'Error code' => $e->getCode(),
                'Error message' => $e->getMessage(),
            ])
            );
        }
    }
}
