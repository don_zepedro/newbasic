<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace app\modules\controllers;
use yii\web\Controller;
use app\models\Records;
//use yii\web\Request;
/**
 * Description of RecordsController
 *
 * @author zepedro
 */
class RecordsController extends Controller {
    //put your code here
    public $enableCsrfValidation = false;//
    
    
    public function actionGetRecords(){
       $request_method = \Yii::$app->request->method;
       if(!($request_method == 'GET')){
           \Yii::$app->response->statusCode = 405;
           return $this->asJson('Error. Wrong request method');
       }
       $request_body = \Yii::$app->request->bodyParams;
       if(isset($request_body['id'])){
            \Yii::$app->response->statusCode = 200;
            return $this->asJson($this->findModel($request_body['id']));
       }else if(empty ($request_body)){
            \Yii::$app->response->statusCode = 200;
            return $this->asJson(Records::find()->all());
           
       }
       \Yii::$app->response->statusCode = 400;
           return $this->asJson('Error.Bad request');
        
    }
    
    public function actionCreateRecords(){
       $request_method = \Yii::$app->request->method;
       if(!($request_method == 'POST')){
           \Yii::$app->response->statusCode = 405;
           return $this->asJson('Error. Wrong request method');
       }
       $request_body = \Yii::$app->request->bodyParams;
       $Record = new Records();
       try{
           $Record->title = $request_body['title'];
           $Record->description = $request_body['description'];
           $Record->save();
           \Yii::$app->response->statusCode = 405;
           return $this->asJson($Record);
       } catch (Exception $ex) {
           \Yii::$app->response->statusCode = 412;
               return $this->asJson(array(
                   'message' => 'There was a problem while saving to the database',
                   'error status code'=>$ex->statusCode));
       }
    }
    
    public function actionUpdateRecords(){
        $request_method = \Yii::$app->request->method;
       if(!($request_method == 'PUT')){
           \Yii::$app->response->statusCode = 405;
           return $this->asJson('Error. Wrong request method');
       }
       $request_body = \Yii::$app->request->bodyParams;
       if(isset($request_body['id'])){
           if(($curent_record = $this->findModel($request_body['id'])) != null)
           {
           if(isset($request_body['title']))  $curent_record->title = $request_body['title'];
           if(isset($request_body['description']))  $curent_record->description = $request_body['description'];
           try{
               $curent_record->save();
           } catch (Exception $ex) {
               \Yii::$app->response->statusCode = 412;
               return $this->asJson(array(
                   'message' => 'There was a problem while saving to the database',
                   'error status code'=>$ex->statusCode));
           }
           \Yii::$app->response->statusCode = 200;
           return $this->asJson($curent_record);
           }
       }
    }
    
    public function actionDeleteRecords()
    {
       $request_method = \Yii::$app->request->method;
//       if(!($request_method == 'DELETE')){
//           \Yii::$app->response->statusCode = 405;
//           return $this->asJson('Error. Wrong request method');
//       }
       $request_body = \Yii::$app->request->bodyParams;
       if(isset($request_body['id'])){
           if($this->findModel($request_body['id']) == null)
           {
               \Yii::$app->response->statusCode = 404;
               return $this->asJson('Error, probably record with that id is no exsist');
           }else{
               $this->findModel($request_body['id'])->delete();
                \Yii::$app->response->statusCode = 200;
                return $this->asJson('The record was succsesfully deleted');
           }
       }else if(empty ($request_body)){
            \Yii::$app->response->statusCode = 400;
           return $this->asJson('Error.Bad request, record id is missing');
           
       }
            \Yii::$app->response->statusCode = 400;
           return $this->asJson('Error.Bad request');
    }
    
    protected function findModel($id)
    {
        if (($model = Records::findOne($id)) !== null) {
            return $model;
        }
        return null;
    }
}
