<?php

namespace app\controllers;
use app\models\Records;
use app\models\Categories;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;



class ByCategoriesController extends \yii\web\Controller
{
    public function actionListbycategories()
    {
        $request_body = \Yii::$app->request->get();
//        var_dump($request_body['rec_id']);
        $records_by_cat = Records::find()->where(['id'=>$request_body['rec_id']])->all();
        
       
        return $this->render('listbycategories', compact('records_by_cat'));
    }

}
